/*
 * simple_thread.cc
 *
 *  Created on: Sep 30, 2017
 *      Author: buttonfly
 */

#include "worker_thread.h"

#include <iostream>

namespace base {

WorkerThread::WorkerThread() {
}

WorkerThread::~WorkerThread() {
}

int WorkerThread::Init(int numof_thread) {
  numof_thread_ = numof_thread;
  return 0;
}

int WorkerThread::Start() {
  for(int i = 0; i < numof_thread_; i++) {
    threads_.push_back(std::thread([&](){
      while(!stopped_) Run();
    }));
  }
  started_ = true;
  return 0;
}

void WorkerThread::Stop() {
  for(auto &thread : threads_) {
    if(thread.native_handle() == pthread_self()) {
      std::cerr << __func__ << " This thread is not allowed to call this method" << std::endl;
      return;
    }
  }

  if(stopped_) return;

  stopped_ = true;
  cond_.notify_all();

  if(started_) {
    for(auto &thread : threads_) {
      thread.join();
    }
  }
}

void WorkerThread::Run() {
  auto task = GetTask();
  if(task) task();
}

int WorkerThread::PostTask(Task task) {
  std::unique_lock<std::mutex> lk(lck_);
  task_queue_.push_back(task);
  cond_.notify_one();
  return 0;
}

WorkerThread::Task WorkerThread::GetTask() {
  std::unique_lock<std::mutex> lk(lck_);
  for(;;) {
    if(! task_queue_.empty()) break;
    cond_.wait(lk);
    if(stopped_) return nullptr;
  }

  auto task = task_queue_.front();
  task_queue_.pop_front();
  return task;
}

} /* namespace base */
