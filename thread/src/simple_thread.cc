/*
 * simple_thread.cc
 *
 *  Created on: Sep 30, 2017
 *      Author: buttonfly
 */

#include "simple_thread.h"

#include <iostream>

namespace base {

SimpleThread::SimpleThread() {
}

SimpleThread::~SimpleThread() {
}

int SimpleThread::Init() {
  return 0;
}

int SimpleThread::Start() {
  thread_ = std::thread([&](){
    while(!stopped_) Run();
  });
  started_ = true;
  return 0;
}

void SimpleThread::Stop() {
  if(thread_.native_handle() == pthread_self()) {
    std::cerr << __func__ << " This thread is not allowed to call this method" << std::endl;
    return;
  }

  if(stopped_) return;

  stopped_ = true;
  cond_.notify_one();

  if(started_) {
    thread_.join();
  }
}

void SimpleThread::Run() {
  auto task = GetTask();
  if(task) task();
}

int SimpleThread::PostTask(Task task) {
  std::unique_lock<std::mutex> lk(lck_);
  task_queue_.push_back(task);
  cond_.notify_one();
  return 0;
}

SimpleThread::Task SimpleThread::GetTask() {
  std::unique_lock<std::mutex> lk(lck_);
  for(;;) {
    if(! task_queue_.empty()) break;
    cond_.wait(lk);
    if(stopped_) return nullptr;
  }

  auto task = task_queue_.front();
  task_queue_.pop_front();
  return task;
}

} /* namespace base */
