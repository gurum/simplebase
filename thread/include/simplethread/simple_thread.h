/*
 * simple_thread.h
 *
 *  Created on: Sep 30, 2017
 *      Author: buttonfly
 */

#ifndef GURUM_BASE_SIMPLE_THREAD_H_
#define GURUM_BASE_SIMPLE_THREAD_H_

#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <list>

namespace base {

class SimpleThread {

using Task=std::function<void()>;

public:
  SimpleThread();
  virtual ~SimpleThread();

  int Init();
  int Start();
  void Stop();

  int PostTask(Task task);

private:
  void Run();
  Task GetTask();

private:
  std::list<Task> task_queue_;

  std::thread thread_;
  std::mutex lck_;
  std::condition_variable cond_;

  bool started_ = false;
  bool stopped_ = false;
};

} /* namespace base */

#endif /* GURUM_BASE_SIMPLE_THREAD_H_ */
