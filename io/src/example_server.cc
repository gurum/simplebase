/*
 ============================================================================
 Name        : exampleProgram.c
 Author      : buttonfly
 Version     :
 Copyright   : Your copyright notice
 Description : Uses shared library to print greeting
               To run the resulting executable the LD_LIBRARY_PATH must be
               set to ${project_loc}/libsimplenet/.libs
               Alternatively, libtool creates a wrapper shell script in the
               build directory of this program which can be used to run it.
               Here the script will be called exampleProgram.
 ============================================================================
 */

#include <simpleio/simple_server.h>
#include <simpleio/multicast_server.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <algorithm>
#include <functional>
#include <sstream>
#include <memory>
#include <list>

using namespace gurum;
using namespace std;

#if GCC_VERSION < 40800 // It is already supported in c++11
//template <typename T>
//static inline std::string to_string(T value) {
//  std::ostringstream stm ;
//  stm << value ;
//  return stm.str();
//}
//
//static inline int stoi(const std::string &value) {
//  return atoi(value.c_str());
//}
#endif

#define OVERRIDE
#define _out_

class Context {
public:
  Context() : server(true) {
  }
  virtual ~Context() {
  }
  string prot; // [ tcp | udp | unix ]
  string port;
  string sckFile; // just name
  string pidFile;
  string progdir;
  bool server; // or client
};

static Context &getcontext() {
  static Context context;
  return context;
}

#define PID_PATH_PREFIX "/tmp/"
//static string _pidfile;
//static string _sckfile;
//static SimpleServer *_srv = NULL;
//static SimpleClient *_clnt = NULL;

static void decode_options(int argc, char *argv[], _out_ Context &context);
static int write_pid(string &file);
static int remove_pid(string &file);
static int init_dir(const char * prefix, const char *progname);
static void deinit_dir();
static void signalhandler( int signo,siginfo_t *siginfo, void *arg);

static const char *buildDate() {
    return __DATE__ " " __TIME__;
}

#if 0
class LocalServerDelegate : public SimpleServerDelegate {
public:
  LocalServerDelegate() {
  }

  virtual ~LocalServerDelegate() {
  }

  void connected(int sck) OVERRIDE {
    printf("%s\n", __PRETTY_FUNCTION__);
  }

  void disconnected(int sck) OVERRIDE {
    printf("%s\n", __PRETTY_FUNCTION__);
  }

  void read(int sck, uint8_t *buf, int len) OVERRIDE {
    printf("%s: %s\n", __PRETTY_FUNCTION__, buf);
  }
};

class LocalClientDelegate : public SimpleClientDelegate {
public:
  LocalClientDelegate() {
  }

  virtual ~LocalClientDelegate() {
  }

  void connected() OVERRIDE {
    printf("%s\n", __PRETTY_FUNCTION__);
  }

  void disconnected() OVERRIDE {
    printf("%s\n", __PRETTY_FUNCTION__);
  }

  void read(uint8_t *buf, int len) OVERRIDE {
    printf("%s\n", __PRETTY_FUNCTION__);
  }
};
#endif


static void init_sig() {
    struct sigaction sig;
  sig.sa_flags = SA_SIGINFO;
  sig.sa_sigaction = signalhandler;
  sigemptyset( &sig.sa_mask);      // No signals to be blocked when a signal is processing.
  sigfillset( &sig.sa_mask);        // All signals will be blocked.

  sig.sa_flags = SA_SIGINFO;
  sigaction(SIGTERM, &sig, 0);
  sigaction(SIGINT, &sig, 0);
  sigaction(SIGSEGV, &sig, 0);
  sigaction(SIGKILL, &sig, 0);

}

static string _progdir;
int init_dir(const char * prefix, const char *progname) {
  // pid
  string path(prefix);
  path.append(progname);
  fprintf(stderr, "%s\n", path.c_str());

  // ex: /var/run/webproxy/$webid/pid
  struct stat st;
  if(stat(path.c_str(),&st) == -1)
    ::mkdir(path.c_str(), 0777);

  path.append("/");
  path.append(to_string(getpid()));

  if(stat(path.c_str(),&st) == -1)
    ::mkdir(path.c_str(), 0777);

//  _progdir = path;
  //e.g  /tmp/exampleProgram/4452
  getcontext().progdir = path;

  string pid(getcontext().progdir);
  pid.append("/pid");
  write_pid(pid);
  return 0;
}

void deinit_dir() {
  string tmp(getcontext().progdir);
  if(! tmp.empty()) {
    tmp.append("/pid");
    fprintf(stderr, "%s will be deleted\n", tmp.c_str());
    unlink(tmp.c_str());
  }

  if( !getcontext().sckFile.empty()) {
    fprintf(stderr, "%s will be deleted\n", getcontext().sckFile.c_str());
    unlink(getcontext().sckFile.c_str());
  }

  if( ! getcontext().progdir.empty()) {
    fprintf(stderr, "%s will be deleted\n", getcontext().progdir.c_str());
    rmdir(getcontext().progdir.c_str());
  }
}

int make_directory(const std::string &path) {
  for(size_t at = 0; ; ++at) {
    at = path.find("/", at);
    if(at==0) continue;

    std::string tmp = path.substr(0, at);
    fprintf(stderr, "%s[%d]\n", tmp.c_str(), at);

    struct stat st;
    if(stat(tmp.c_str(),&st) == -1)
      ::mkdir(tmp.c_str(), 0777);

    if(at==std::string::npos) break;
  }
  return 0;
}

int main(int argc, char *argv[]) {
    fprintf(stderr,
        "%s (C)2001-2011, gurumlab \n"
    "  Built on %s\n", argv[0], buildDate());

    make_directory("/tmp/nfbe/audio-mgmt");
    return 0;


    std::unique_ptr<gurum::SimpleServer> server;

    decode_options(argc, argv, getcontext());

    // signal
    init_sig();

    init_dir(PID_PATH_PREFIX, basename(argv[0]));

  if(getcontext().prot.compare("tcp") == 0) {
    Context &c = getcontext();
    server.reset(new SimpleTcpServer);
    SimpleTcpServer *srv = static_cast<SimpleTcpServer *>(server.get());
    srv->SetPort((uint16_t)std::stoi(c.sckFile));
    srv->SetTimeout(10);
    srv->SetCallback([](int sck){
      fprintf(stderr, "connected\n");
    },
    [] (int) {
      fprintf(stderr, "disconnected\n");
    },
    [](int sck, uint8_t *buf, int len) {
      fprintf(stderr, "read %s\n", buf);
    });

    if(srv->Start() == 0) {
      string c;
      for(;;) {
        getline(cin, c);
        srv->Send((uint8_t *)c.c_str(), c.size());
      }
    }
  }
  else if(getcontext().prot.compare("udp") == 0) {
    Context &c = getcontext();
    server.reset(new MulticastServer);
    MulticastServer *srv = static_cast<MulticastServer *>(server.get());
    srv->SetCallback([](int sck){
      fprintf(stderr, "connected\n");
    },
    [] (int) {
      fprintf(stderr, "disconnected\n");
    },
    [](int sck, uint8_t *buf, int len) {
      fprintf(stderr, "read %s\n", buf);
    });

    srv->SetPort((uint16_t)stoi(c.sckFile));
    srv->SetIP("224.1.1.200");
    srv->SetAlias("ens33");
    srv->SetTTL(1);
//      _srv->setDelegate(new LocalServerDelegate);
    if(srv->Start() == 0) {
      string c;
      for(;;) {
        getline(cin, c);
        srv->Send((uint8_t *)c.c_str(), c.size());
      }
    }
  }
  else if(getcontext().prot.compare("unix") == 0) {
    Context &c = getcontext();
    if(c.sckFile.empty()) {
      string tmp(getcontext().progdir);
      tmp.append("/.sck");
      getcontext().sckFile = tmp;
      fprintf(stderr, "sck: %s\n", tmp.c_str());
    }

    unlink(c.port.c_str());
    server.reset(new SimpleUnixServer);
    SimpleUnixServer *srv = static_cast<SimpleUnixServer *>(server.get());
//      _srv->setDelegate(new LocalServerDelegate);
    srv->SetCallback([](int sck){
      fprintf(stderr, "connected\n");
    },
    [] (int) {
      fprintf(stderr, "disconnected\n");
    },
    [](int sck, uint8_t *buf, int len) {
      fprintf(stderr, "read %s\n", buf);
    });

    ((SimpleUnixServer *)srv)->SetUnixSockPath(getcontext().sckFile);
    srv->SetTimeout(10);
    if(srv->Start() == 0) {
      string c;
      for(;;) {
        getline(cin, c);
        srv->Send((uint8_t *)c.c_str(), c.size());
      }
    }
  }
  else {
    fprintf(stderr, "Unknown protocol\n");
    exit(0);
  }


  if(server) {
    server->Stop();
    server.reset(nullptr);
  }

  deinit_dir();

  return 0;
}


static void usage(const char* program) {
  printf("Usage: %s <options>\n", program);
  printf("Options:\n"
      "\t-h, -help\n"
      "\t\tPrint this help\n"
      "\t-c, --client\n"
      "\t\trun as client\n"
      "\t-p, --port <1~65525>\n"
      "\t\tport. It might be a file if it is unix domain sock.\n"
      "\tex: %s tcp -p 8085\n"
      "\tex: %s unix -p /tmp/.sck\n", program ,program
  );
}

static void decode_options(int argc, char *argv[], Context &context) {
  static const char *opt_string = "p:ch";
  static struct option const longopts[] = {
      { "port", required_argument, NULL,'p' },
      { "client", no_argument, NULL,'c' },
      { "help", no_argument, NULL, 'h' },
      { NULL, 0, NULL, 0 } };

    int optc, longind=0;
    const char *name = argv[0];

    while((optc=getopt_long(argc,argv,opt_string,longopts,&longind))!=-1) {
        switch (optc)
        {
        case 'h':
          usage(name);
          exit(0);

        case 'p':
          getcontext().sckFile = optarg;
          break;

        case 'c':
          getcontext().server = false; // client
          break;

        default:
          usage(name);
          exit(0);
        }
    }

   for(int i = optind; i < argc; i++) {
      fprintf(stderr, "argv[%d]: %s\n", i, argv[i]);
      //TODO
      getcontext().prot = argv[i];
    }
}


int write_pid(string &file) {
  FILE* fp = NULL;
  fp = fopen(file.c_str(), "w");
  if(fp == NULL) {
    fprintf(stderr, "failed to open %s.", file.c_str());
    return -1;
  }
  fprintf(fp, "%d\n", getpid());
  fclose(fp);
  return 0;
}

int remove_pid(string &file) {
  if(file.empty()) return -1;

  unlink(file.c_str());
  return 0;
}

void signalhandler( int signo,siginfo_t *siginfo, void *arg) {
  sigset_t    sigset, oldset;
  sigfillset(&sigset);
  sigprocmask(SIG_BLOCK, &sigset, &oldset);

    switch(signo) {
    case SIGTERM:
    case SIGINT:
    case SIGSEGV:
    case SIGHUP:
    case SIGKILL: {
//      remove_pid(_pidfile);
      fprintf(stderr, "%s will be deleted\n", _progdir.c_str());
      deinit_dir();
      exit(1);
      break;
    }}
  sigprocmask(SIG_SETMASK, &oldset, NULL);
}

