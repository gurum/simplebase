/*
 * MulticastClient.cc
 *
 *  Created on: May 1, 2016
 *      Author: buttonfly
 */

#include "multicast_client.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <assert.h>

namespace gurum {

MulticastClient::MulticastClient()
: _port(0)
{
}

MulticastClient::~MulticastClient() {
	// TODO Auto-generated destructor stub
}

int MulticastClient::Open() {
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	assert(fd > 0);

  memset(&_serveraddr, 0, sizeof(_serveraddr));
  _serveraddr.sin_family = AF_INET;
  _serveraddr.sin_addr.s_addr = inet_addr(_ip.c_str());
  _serveraddr.sin_port = htons(_port);
  bind(fd, (struct sockaddr *)&_serveraddr, sizeof(struct sockaddr));

	// join group
	memset(&_mreq, 0, sizeof(_mreq));
	_mreq.imr_multiaddr.s_addr = inet_addr(_ip.c_str());
	_mreq.imr_interface.s_addr = INADDR_ANY;
	setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&_mreq, sizeof(_mreq));

	struct timeval tv;
	memset(&tv, 0, sizeof(tv));
	tv.tv_sec = 5; // TODO
	setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO,(struct timeval *)&tv,sizeof(struct timeval));
	setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO,(struct timeval *)&tv,sizeof(struct timeval));
	return fd;

exception:
	close(fd);
	return -1;
}

int MulticastClient::Recv(int fd, uint8_t *buf, size_t len) {
	int addrlen = sizeof(_serveraddr);
 	return recvfrom(fd, (void *)buf, len, 0, ( struct sockaddr *)&_serveraddr,  (socklen_t *)&addrlen);
}

void MulticastClient::Run()  {
	uint8_t buf[32] = {0};
	int n = Recv(_sck, buf, sizeof(buf));
	if(n > 0)
	  fprintf(stderr, "%d\n !!", n);
}

} /* namespace gurum */


//extern "C"

void *multicast_client_new() {
  gurum::MulticastClient *clnt = new gurum::MulticastClient;
  return clnt;
}

void multicast_client_set_ip(void *handle, const char *addr) {
  gurum::MulticastClient *clnt = static_cast<gurum::MulticastClient *>(handle);
  clnt->SetIP(addr);
}

void multicast_client_set_port(void *handle, unsigned short port) {
  gurum::MulticastClient *clnt = static_cast<gurum::MulticastClient *>(handle);
  clnt->SetPort(port);
}

int multicast_client_start(void *handle) {
  gurum::MulticastClient *clnt = static_cast<gurum::MulticastClient *>(handle);
  return clnt->Start();
}

void multicast_client_stop(void *handle) {
  gurum::MulticastClient *clnt = static_cast<gurum::MulticastClient *>(handle);
  clnt->Stop();
}

void multicast_client_delete(void *handle) {
  gurum::MulticastClient *clnt = static_cast<gurum::MulticastClient *>(handle);
  delete clnt;
}

void multicast_client_set_connected_callback(void *handle,std::function<void(void)> cb) {
  gurum::MulticastClient *clnt = static_cast<gurum::MulticastClient *>(handle);
  clnt->SetConnectedCallback(cb);
}

void multicast_client_set_disconnected_callback(void *handle,std::function<void(void)> cb) {
  gurum::MulticastClient *clnt = static_cast<gurum::MulticastClient *>(handle);
  clnt->SetDisconnectedCallback(cb);
}

void multicast_client_set_hook_callback(void *handle, std::function<bool(int,int)> cb) {
  gurum::MulticastClient *clnt = static_cast<gurum::MulticastClient *>(handle);
  clnt->SetHookCallback(cb);
}

void multicast_client_set_read_callback(void *handle, std::function<void(uint8_t*,int)> cb) {
  gurum::MulticastClient *clnt = static_cast<gurum::MulticastClient *>(handle);
  clnt->SetReadCallback(cb);
}

