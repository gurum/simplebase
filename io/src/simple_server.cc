/*
 * SimpleServer.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#include "simple_server.h"

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <assert.h>
#include <sys/time.h>
#include <sys/select.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#define PID_PATH "/tmp/"

using namespace std;

namespace gurum {

using Buffer=std::unique_ptr<uint8_t, std::function<void(uint8_t *)>>;

SimpleServer::SimpleServer()
: _delegate(NULL), _stopped(false), _started(false), _sck(-1), _timeout(0)
{
#if USE_CC_THREAD
#else
	pthread_mutexattr_t mutexAttr;
	pthread_mutexattr_init(&mutexAttr);

	pthread_mutexattr_settype(&mutexAttr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutexattr_settype(&mutexAttr, PTHREAD_MUTEX_FAST_NP);

	pthread_mutex_init(&_lck, &mutexAttr);
	pthread_mutexattr_destroy(&mutexAttr);
#endif
}

SimpleServer::~SimpleServer() {
	if(! _stopped) Stop();
}

int SimpleServer::Start() {
	_sck = Open();
	if(_sck < 0) {
		fprintf(stderr, "failed to open a socket\n");
		return -1;
	}

	_fd_max = _sck;

	FD_ZERO(&_readfds);
	FD_SET(_sck, &_readfds);

#if USE_CC_THREAD
	_thread = std::thread([&](){
		while(!_stopped) Run(0);
	});
#else
    pthread_attr_t        attribute;
    pthread_attr_init(&attribute);
    pthread_attr_setscope(&attribute, PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setdetachstate(&attribute, PTHREAD_CREATE_JOINABLE);
	pthread_create(&_thread, &attribute, (void* (*)(void*)) thr, (void*)this);
	pthread_attr_destroy(&attribute);
 #endif
	_started = true;
	return 0;
}

#if USE_CC_THREAD
#else
void SimpleServer::thr(void *arg) {
	SimpleServer *self = (SimpleServer *) arg;
	while(!self->_stopped) self->Run(0);
}
#endif

int SimpleServer::Send(uint8_t *buf, size_t len) {
  fd_set tmpfds;
  tmpfds = _readfds;

  for(int fd=0; fd < _fd_max + 1; fd++) {
    if(! FD_ISSET(fd, &tmpfds)) continue;

    if(fd==_sck)
      continue;

    int n = write(fd, buf, len);
    // TODO:
  }
  return 0;
}

int SimpleServer::ReadAndDispatch() {
  if(_sck < 0) {
    _sck = Open();
    if(_sck < 0) return -1;

    _fd_max = _sck;

    FD_ZERO(&_readfds);
    FD_SET(_sck, &_readfds);
  }

  Run(0);
  return 0;
}

void SimpleServer::Run(int unused) {
	int clntsck;
	fd_set tmpfds;
	int clntaddr_len = SockaddrLen();
	tmpfds = _readfds;

	struct timeval tv;
	struct timeval *p_tv;

	if(_timeout > 0) {
		tv.tv_sec = _timeout / 1000;
		tv.tv_usec = (_timeout % 1000) * 1000;
		p_tv = &tv;
	}
//	_timeout
	int stat;
   	do
   		stat = select(_fd_max + 1, &tmpfds, NULL, NULL, p_tv);
   	while (EINTR == errno);

	if(stat == 0) { // time-over
	}

	for(int fd=0; fd < _fd_max + 1; fd++) {
		if(! FD_ISSET(fd, &tmpfds)) continue;

		if(fd == _sck) {
			clntsck = accept(fd, Sockaddr(), (socklen_t*) &clntaddr_len);
			if(clntsck < 0) continue;

			if(clntsck > _fd_max) _fd_max = clntsck;

			FD_SET(clntsck, &_readfds);
			if(on_connected_) on_connected_(clntsck);

			if(_delegate) _delegate->connected(clntsck);
		}
		else {
			int n;
			if(ioctl(fd, FIONREAD, &n) < 0) continue;

			if(n==0) {
				FD_CLR(fd, &_readfds);
				if(on_disconnected_) on_disconnected_(fd);

				if(_delegate) _delegate->disconnected(fd);
			}
			else if(n > 0) {
				Buffer buf{
            		(uint8_t *) malloc(n + 1),
            		[](uint8_t *ptr) { if(ptr) { free(ptr);}}
        		};

				if(! buf) {
					fprintf(stderr, "E: failed to malloc\n");
					continue;
				}
				memset(buf.get(), 0, n + 1);
				n = read(fd, buf.get(), n);
				if(on_read_) on_read_(fd, (uint8_t *)buf.get(), n);

				if(_delegate) _delegate->read(fd, (uint8_t *)buf.get(), n);

			}
		}
	}
}

void SimpleServer::Stop() {
#if USE_CC_THREAD
	if(_thread.native_handle() == pthread_self()) {
		fprintf(stderr, "W: This thread is not allowed to call this method\n");
		return;
	}
#else
	if(_thread == pthread_self()) {
		fprintf(stderr, "W: This thread is not allowed to call this method\n");
		return;
	}
#endif

	if(_stopped) {
		fprintf(stderr, "W: It's already stopped.\n");
		return;
	}

	_stopped = true;

	if(_started) {
#if USE_CC_THREAD
		_thread.join();
#else
		int status;
		pthread_join(_thread, (void **)&status);
#endif
	}

	// no thread for now.
	if(_sck > 0) close(_sck);
	_sck = -1;
}

void SimpleServer::lock() {
#if USE_CC_THREAD
	_lck.lock();
#else
	pthread_mutex_lock(&_lck);
#endif
}

void SimpleServer::unlock() {
#if USE_CC_THREAD
	_lck.unlock();
#else
	pthread_mutex_unlock(&_lck);
#endif
}



} /* namespace gurum */
