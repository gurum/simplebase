/*
 * SimplePipeServer.cc
 *
 *  Created on: Feb 11, 2017
 *      Author: buttonfly
 */

#include "simple_server.h"
#include <unistd.h>
#include <fcntl.h>
#if __linux__
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <limits.h>
#include <signal.h>
#include <assert.h>
#endif

namespace gurum {

SimplePipeServer::SimplePipeServer() {
}

SimplePipeServer::~SimplePipeServer() {
}

int SimplePipeServer::Open() {

	if(!_path.empty()) {
		//
#if __linux__
		unlink(_path.c_str());
		int err = mkfifo(_path.c_str(), 0666);
		if(err) {
			fprintf(stderr, "E: failed to mkfifo %s\n", _path.c_str());
			return -1;
		}

		_fd = ::open(_path.c_str(), O_RDWR);
		if(_fd < 0) {
			fprintf(stderr, "E: failed open : %s\n", _path.c_str());
		}

		_id = inotify_init();
		assert(_id>0);
		_wd = inotify_add_watch(_id, _path.c_str(), IN_OPEN|IN_CLOSE);
		assert(_wd>0);
#endif
	}
	return _fd;
}

void SimplePipeServer::Stop() {
	SimpleServer::Stop();

#ifdef __linux__
	inotify_rm_watch(_id, _wd);
	close(_id);
#endif
	if(! _path.empty()) {
		::unlink(_path.c_str());
		_path = "";
	}
}

void SimplePipeServer::Run(int clnt) {
#ifdef __linux__
#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))
	int n;
	char buf[BUF_LEN] __attribute__ ((aligned(8)));
	n = read(_id, buf, BUF_LEN);

	if(n==0) {
		// error
	}

	if(n==-1) {
		// error
	}


	char *ptr;
	struct inotify_event *event;
	for(ptr = buf; ptr < buf + n; ptr += sizeof(struct inotify_event) + event->len) {
		event = (struct inotify_event *) ptr;
		// TODO
		if (event->mask & IN_CLOSE_NOWRITE) {
			if(_delegate)
				_delegate->disconnected(0);
		}
		if (event->mask & IN_OPEN)   {
			if(_delegate)
				_delegate->connected(0);
		}
	}
#endif
}

int SimplePipeServer::Send(uint8_t *buf, size_t len) {
	return write(_fd, buf, len);
}


} /* namespace gurum */
