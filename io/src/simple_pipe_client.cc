/*
 * SimplePipeClient.cc
 *
 *  Created on: Feb 11, 2017
 *      Author: buttonfly
 */

#include "simple_client.h"
#include <unistd.h>
#include <fcntl.h>

namespace gurum {

SimplePipeClient::SimplePipeClient() // default: stdin
{
}

SimplePipeClient::~SimplePipeClient() {
	// TODO Auto-generated destructor stub
}

int SimplePipeClient::Open() {

	if(! _path.empty()) {
#if __linux__
		_sck = ::open(_path.c_str(), O_RDONLY);
		if(_sck < 0) {
			fprintf(stderr, "failed to open: %s\n", _path.c_str());
			return -1;
		}
#endif
	}
	else {
		_sck = 0; // default: stdin
	}
	return _sck;
}

} /* namespace gurum */
