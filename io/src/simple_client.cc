/*
 * SimpleClient.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#include "simple_client.h"

#include <sys/un.h>
#include <sys/socket.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#define m_(...)
#define S_

namespace gurum {

using Buffer=std::unique_ptr<uint8_t, std::function<void(uint8_t *)>>;

SimpleClient::SimpleClient()
: _stopped(false), _delegate(nullptr), _sck(-1), _timeout(0)
{
}

SimpleClient::~SimpleClient() {
	if(! _stopped) Stop();
}

int SimpleClient::Start() {
	_sck = Open();
	assert(_sck >= 0);

#if USE_CC_THREAD
	_thread = std::thread([&](){
		for(; ! _stopped ;) Run();
	});
#else
    pthread_create(&_thread, NULL, (void* (*)(void*)) thr, (void*)this);
#endif

  _started = true;
	return 0;
}

void SimpleClient::Stop() {
	_stopped = true;
#if USE_CC_THREAD
	if(_started) _thread.join();
#else
	pthread_join(_thread, NULL);
#endif
	if(_sck > 0)
		close(_sck);
	_sck = -1;
}

int SimpleClient::ReadAndDispatch() {
  if(_sck < 0)
    _sck = Open();

  if(_sck < 0) return -1;

  Run();
  return 0;
}

void SimpleClient::Run() {

	int ret = -1;
	int rc = -1;
	fd_set readfds;
	struct timeval tv = { 0 };
	struct timeval *p_tv = NULL;

	assert(_sck >= 0);
	FD_ZERO(&readfds);
	FD_SET(_sck, &readfds);

	if(_timeout > 0) {
		tv.tv_sec = _timeout / 1000;
		tv.tv_usec = (_timeout % 1000) * 1000;
		p_tv = &tv;
	}

	int stat = select(_sck+1, &readfds, NULL, NULL, p_tv);
	if(stat == 0) { // time-over
	}
	else if (0 < stat) {
		int n;
		if(ioctl(_sck, FIONREAD, &n) < 0)
			return;

		if(n==0) {
			FD_CLR(_sck, &readfds);
      if(on_disconnected_) on_disconnected_();

      //@deprecated
			if(_delegate) _delegate->disconnected();

		}
		else if(n > 0) {
		  if(on_hook_ && on_hook_(_sck, n)) return;

			Buffer buf{
	    		(uint8_t *) malloc(n + 1),
	    		[](uint8_t *ptr) { if(ptr) { free(ptr);}}
			};

      memset(buf.get(), 0, n + 1);
      assert(buf);
      n = this->Recv(_sck, buf.get(), n);

      if(on_read_) on_read_((uint8_t *)buf.get(), n);

      // if(buf) free(buf);

      // @deprecated
			if(_delegate) {
				// if returning true,
				// It means that a user will read for himself.
				if(_delegate->hook(_sck, n)) return;

				uint8_t *buf = (uint8_t *) malloc(n + 1);
				memset(buf, 0, n + 1);
				assert(buf);
				n = this->Recv(_sck, buf, n);

				if(_delegate) _delegate->read((uint8_t *)buf, n);

				if(buf) free(buf);
			}
		}
	}
	/* Else it timed out */
	//    printf("\nExiting : %s\n", __func__);
}

#if USE_CC_THREAD
#else
void SimpleClient::thr(void *arg) {
	SimpleClient *self = (SimpleClient *) arg;
	for(; ! self->_stopped ;) self->Run();
}
#endif

int SimpleClient::Send(void *buf, int len) {
	return write(_sck, buf, len);
}

int SimpleClient::Send(string &msg) {
	const char *tmp = msg.c_str();
	return Send((void *) tmp, (int) msg.size());
}

int SimpleClient::Send(const char *msg) {
	return Send((void *) msg, strlen(msg));
}

int SimpleClient::Recv(int fd, uint8_t *buf, size_t len) {
	return read(fd, buf, len);
}
} /* namespace gurum */
