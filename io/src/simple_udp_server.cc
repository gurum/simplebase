/*
 * SimpleUdpServer.cc
 *
 *  Created on: May 3, 2016
 *      Author: buttonfly
 */

#include "simple_server.h"
#include <stdio.h>
#include <unistd.h>

using namespace std;

namespace gurum {

SimpleUdpServer::SimpleUdpServer()
{
}

SimpleUdpServer::~SimpleUdpServer() {
}

int SimpleUdpServer::Open() {
	 int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (fd  < 0) {
		fprintf(stderr, "failed to create a socket descriptor\n");
		return -1;
	}

	memset(&_serveraddr, 0, sizeof(_serveraddr));
	_serveraddr.sin_family = AF_INET;
	_serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	_serveraddr.sin_port = htons(_port);

	if(::bind(fd, (struct sockaddr*) &_serveraddr, sizeof(_serveraddr)) < 0) {
		fprintf(stderr, "failed to bind\n");
		goto exception;
	}

	if(::listen(fd, 5) < 0) {
		fprintf(stderr, "failed to listen\n");
		goto exception;
	}
	return fd;

exception:
	close(fd);
	return -1;
}

} /* namespace gurum */
