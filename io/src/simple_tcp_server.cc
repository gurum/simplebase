/*
 * SimpleTcpServer.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#include "simple_server.h"
#include <stdio.h>

using namespace std;

namespace gurum {

SimpleTcpServer::SimpleTcpServer()
:_port(0)
{
}

SimpleTcpServer::~SimpleTcpServer() {
}

int SimpleTcpServer::Open() {
	 int fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (fd  < 0) {
		fprintf(stderr, "failed to create a socket descriptor\n");
		return -1;
	}

	memset(&_serveraddr, 0, sizeof(_serveraddr));
	_serveraddr.sin_family = AF_INET;
	_serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	_serveraddr.sin_port = htons(_port);

	if(::bind(fd, (struct sockaddr*) &_serveraddr, sizeof(_serveraddr)) < 0) {
		fprintf(stderr, "failed to bind\n");
	}

	if(::listen(fd, 5) < 0) {
		fprintf(stderr, "failed to listen\n");
	}
	return fd;
}

struct sockaddr *SimpleTcpServer::Sockaddr()  {
	return (struct sockaddr *) &_clntaddr;
}

size_t SimpleTcpServer::SockaddrLen()  {
	return sizeof(struct sockaddr_in);
}

} /* namespace gurum */
