/*
 * example_non_thread_server.cc
 *
 *  Created on: Oct 1, 2017
 *      Author: buttonfly
 */

#include <simpleio/simple_server.h>
#include <string>
#include <memory>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <functional>
#include <sstream>

using namespace std;
using namespace gurum;

int main(int argc, char *argv[]) {

  unique_ptr<SimpleUnixServer> server{new SimpleUnixServer};
  server->SetUnixSockPath(".sck");
  server->SetTimeout(10);
  server->SetCallback([](int sck){
    fprintf(stderr, "connected\n");
  },
  [] (int) {
    fprintf(stderr, "disconnected\n");
  },
  [](int sck, uint8_t *buf, int len) {
    fprintf(stderr, "read %s\n", buf);
  });

#if 1
  for(;;) {
    string c;
    server->ReadAndDispatch();
    getline(cin, c);
    server->Send((uint8_t *)c.c_str(), c.size());
  }
#else
  if(server->Start() == 0) {
    string c;
    for(;;) {
      getline(cin, c);
      server->Send((uint8_t *)c.c_str(), c.size());
    }
  }
#endif


  return 0;
}


