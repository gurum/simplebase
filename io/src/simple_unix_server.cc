/*
 * SimpleUnixServer.cc
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#include <unistd.h>
#include "simple_server.h"
#include <stdio.h>

using namespace std;

namespace gurum {

SimpleUnixServer::SimpleUnixServer() {
}

SimpleUnixServer::~SimpleUnixServer() {
}

int SimpleUnixServer::Open() {
	if(unix_sock_path_.empty()) {
		fprintf(stderr, "You need to call setUnixSockPath()\n");
		return -1;
	}

	 int fd = socket(PF_UNIX, SOCK_STREAM, 0);
	if (fd  < 0) {
		fprintf(stderr, "failed to create a socket descriptor\n");
		return -1;
	}

	memset(&_serveraddr, 0, sizeof(_serveraddr));
	_serveraddr.sun_family = AF_UNIX;
	strcpy(_serveraddr.sun_path, unix_sock_path_.c_str());

	if(::bind(fd, (struct sockaddr*) &_serveraddr, sizeof(_serveraddr)) < 0) {
		fprintf(stderr, "failed to bind\n");
	}

	if(::listen(fd, 5) < 0) {
		fprintf(stderr, "failed to listen\n");
	}
	return fd;
}

void SimpleUnixServer::Stop() {
	SimpleServer::Stop();
	if(! unix_sock_path_.empty()) {
		::unlink(unix_sock_path_.c_str());
		unix_sock_path_ = "";
	}
}

struct sockaddr *SimpleUnixServer::Sockaddr()  {
	return (struct sockaddr *) &_clntaddr;
}

size_t SimpleUnixServer::SockaddrLen()  {
	return sizeof(struct sockaddr_un);
}

} /* namespace gurum */
