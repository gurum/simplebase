/*
 * SimpleUdpClient.cc
 *
 *  Created on: May 3, 2016
 *      Author: buttonfly
 */



#include "simple_client.h"
#include <sys/un.h>
#include <sys/socket.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>

namespace gurum {

SimpleUdpClient::SimpleUdpClient() {
}

SimpleUdpClient::~SimpleUdpClient() {
}

int SimpleUdpClient::Open() {
	int 	fd;
	struct sockaddr_in serveraddr;
	fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(fd < 0) {
		return -1;
	}
    bzero(&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(_port);
    int ret = ::connect(fd, (struct sockaddr *) &serveraddr, sizeof(serveraddr));
	if(ret == -1) {
		goto exception;
	}

  if(on_connected_) on_connected_();

	if(_delegate) _delegate->connected();
	return fd;

exception:
	close(fd);
	return -1;
}

}  /* namespace gurum */
