/*
 * NetUt.cc
 *
 *  Created on: May 1, 2016
 *      Author: buttonfly
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include "netut.h"
#include <net/if.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
//#include <linux/rtnetlink.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>
//#include <asm/types.h>
#if __linux__
#include <netinet/ether.h>
#endif
#include <netinet/in.h>
#include <syslog.h>
#include <net/if.h>
#include <sys/types.h>
#include <net/if.h>
//#include <linux/netlink.h>
#include <dirent.h>
#include <assert.h>

using namespace std;

namespace gurum {

NetUt::NetUt() {}

NetUt::~NetUt() {}

uint32_t NetUt::IP(const char* alias) {
	struct ifreq ifr;
	struct sockaddr_in *sin;
	int fd;
	unsigned long ipaddr;

	if((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("socket");
		return 0;
	}

	memset(&ifr, 0, sizeof(ifr));
	strncpy(ifr.ifr_name, alias, sizeof(ifr.ifr_name));

	if(ioctl(fd, SIOCGIFADDR, &ifr) < 0) {
		close(fd);
		perror("ioctl");
	return 0;
	}

	sin = (struct sockaddr_in *)&(ifr.ifr_addr);
	ipaddr = sin->sin_addr.s_addr;
	close(fd);

	return ipaddr;
}

uint32_t NetUt::defaultIP() {
	unsigned long ipaddr;
	struct if_nameindex *ifs = if_nameindex();    /* retrieve the current interfaces */
	assert(ifs);
	struct if_nameindex *ptr = ifs;
	if(ifs) {
#if 0
		for(struct if_nameindex *ptr = ifs; ptr->if_name; ptr++) {
			fprintf(stderr, "\t%s: %x\n", ptr->if_name, NetUt::IP(ptr->if_name));
		}
#endif
//		ipaddr = NetUt::IP(ifs->if_name);
		if_freenameindex(ifs);
	}
	return ipaddr;
}

string NetUt::dottedIP(const char *ip) {
	uint32_t addr = IP(ip);
	char buf[32];
	struct in_addr _ip;
	_ip.s_addr = addr;
	inet_ntop(AF_INET, &_ip, buf, sizeof(buf));
	return buf;
}

bool NetUt::existNetworkAlias(const char *alias) {
	bool exist = false;
	struct if_nameindex *ifs = if_nameindex();    /* retrieve the current interfaces */
	if(ifs) {
		for(struct if_nameindex *ptr = ifs;ptr->if_name; ptr++) {
			if(memcmp(ptr->if_name, alias, strlen(alias))==0) {
				exist = true;
				break;
			}
		}
		if_freenameindex(ifs);
	}
	return exist;
}

#if __linux__
int NetUt::hwaddr(uint8_t addr[6]) {
	struct ifreq ifr;
    struct ifreq *IFR;
    struct ifconf ifc;
    char buf[1024];
    int s, i;
    int ok = 0;

    s = socket(AF_INET, SOCK_DGRAM, 0);
    if (s==-1) {
        return -1;
    }

    ifc.ifc_len = sizeof(buf);
    ifc.ifc_buf = buf;
    ioctl(s, SIOCGIFCONF, &ifc);

    IFR = ifc.ifc_req;
    for (i = ifc.ifc_len / sizeof(struct ifreq); --i >= 0; IFR++) {
        strcpy(ifr.ifr_name, IFR->ifr_name);
        if (ioctl(s, SIOCGIFFLAGS, &ifr) == 0) {
            if (! (ifr.ifr_flags & IFF_LOOPBACK)) {
                if (ioctl(s, SIOCGIFHWADDR, &ifr) == 0) {
                    ok = 1;
                    break;
                }
            }
        }
    }

    close(s);
    if (ok) {
        bcopy( ifr.ifr_hwaddr.sa_data, addr, ETH_ALEN);
    }
    else {
        return -1;
    }
    return 0;
}
#endif

bool NetUt::isMulticast(const struct sockaddr_storage *saddr, socklen_t len) {
    const struct sockaddr *addr = (const struct sockaddr *) saddr;
    switch(addr->sa_family) {
#if defined(IN_MULTICAST)
        case AF_INET: {
            const struct sockaddr_in *ip = (const struct sockaddr_in *)saddr;
            if ((size_t)len < sizeof (*ip))
                return false;
            return IN_MULTICAST(ntohl(ip->sin_addr.s_addr)) != 0;
        }
#endif
#if defined(IN6_IS_ADDR_MULTICAST)
        case AF_INET6: {
            const struct sockaddr_in6 *ip6 = (const struct sockaddr_in6 *)saddr;
            if ((size_t)len < sizeof (*ip6))
                return false;
            return IN6_IS_ADDR_MULTICAST(&ip6->sin6_addr) != 0;
        }
#endif
    }
    return false;
}

uint32_t peeraddr(int clntsck) {
	struct sockaddr_in addr;
	socklen_t socklen = sizeof(addr); // important! Must initialize length, garbage produced otherwise
	if (getpeername(clntsck, (struct sockaddr*) &addr, &socklen) < 0) {
	   	fprintf(stderr, "failed to  [getpeername]");
	   	return -1;
	}
#if 0
	else {
	    fprintf(stderr, "Address: %s, port: %d\n", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
	}
#endif
    return addr.sin_addr.s_addr;
}

uint32_t sockaddr(int sock) {
	struct sockaddr_in addr;
	socklen_t socklen = sizeof(addr); // important! Must initialize length, garbage produced otherwise
	if (getsockname(sock, (struct sockaddr*) &addr, &socklen) < 0) {
	   	fprintf(stderr, "failed to  [getsockname]");
	   	return -1;
	}
    return addr.sin_addr.s_addr;
}


} /* namespace gurum */
