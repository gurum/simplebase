/*
 * SimpleClient.h
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#ifndef GURUM_SIMPLECLIENT_H_
#define GURUM_SIMPLECLIENT_H_

#define USE_CC_THREAD 1

#ifndef GCC_VERSION
#define GCC_VERSION (__GNUC__ * 10000 \
				 + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)
#endif

#if GCC_VERSION > 40800
#define USE_CC_THREAD	1
#endif

#if USE_CC_THREAD
#include <thread>
#else
#include <pthread.h>
#endif
#include <string>
#include <functional>
#include <cstdint>
//#include <stdint.h>

using namespace std;

namespace gurum {

class SimpleClientDelegate {
public:
	virtual void connected(){}
	virtual void disconnected(){}
	virtual bool hook(int sck, int len){return false;}
	virtual void read(uint8_t *buf, int len){}
};

class SimpleClient {


using OnConnected=std::function<void(void)>;
using OnDisconnected=std::function<void(void)>;
using OnHook=std::function<bool(int,int)>;
using OnRead=std::function<void(uint8_t*,int)>;

public:
	SimpleClient();
	virtual ~SimpleClient();

	void setDelegate(SimpleClientDelegate *delegate) {
		_delegate = delegate;
	}

	void SetConnectedCallback(OnConnected on_connected) {
	  on_connected_=on_connected;
	}
	void SetDisconnectedCallback(OnDisconnected on_disconnected) {
	  on_disconnected_=on_disconnected;
	}
	void SetHookCallback(OnHook on_hook) {
	  on_hook_=on_hook;
	}
	void SetReadCallback(OnRead on_read) {
	  on_read_=on_read;
	}

	void SetCallback(OnConnected on_connected, OnDisconnected on_disconnected, OnHook on_hook, OnRead on_read) {
	  SetConnectedCallback(on_connected);
	  SetDisconnectedCallback(on_disconnected);
	  SetHookCallback(on_hook);
	  SetReadCallback(on_read);
	}

	int Start();
	void Stop();

	int Send(void *buf, int len);
	int Send(string &msg);
	int Send(const char *msg);
	void SetTimeout(unsigned long ms) {
		_timeout = ms;
	}

	// You must not call Start();
	int ReadAndDispatch();

protected:
	virtual int Open()=0;
	virtual int Recv(int fd, uint8_t *buf, size_t len);

protected:
	virtual void Run();
#if USE_CC_THREAD
#else
	static void thr(void *arg);
#endif

protected:
	bool _started=false;
	bool _stopped=false;
#if USE_CC_THREAD
	thread _thread;
#else
	pthread_t _thread;
#endif
	SimpleClientDelegate *_delegate;

	int _sck=-1;
	unsigned long _timeout;

	OnConnected on_connected_=nullptr;
	OnDisconnected on_disconnected_=nullptr;
	OnHook on_hook_=nullptr;
	OnRead on_read_=nullptr;
};

class SimpleTcpClient : public SimpleClient {
public:
	SimpleTcpClient();
	virtual ~SimpleTcpClient();
	void SetPort(uint16_t port) {
		_port = port;
	}

protected:
	virtual int Open();

protected:
	uint16_t _port;
};

class SimpleUdpClient : public SimpleTcpClient {
public:
	SimpleUdpClient();
	virtual ~SimpleUdpClient();

protected:
	virtual int Open();
};

class SimpleUnixClient : public SimpleClient {
public:
	SimpleUnixClient();
	virtual ~SimpleUnixClient();

	void SetUnixSockPath(const string unixSockPath) {
		_unixSockPath = unixSockPath;
	}

protected:
	virtual int Open();

private:
	string _unixSockPath;
//	struct sockaddr_un _serveraddr;
};

class SimplePipeClient : public SimpleClient {
public:
	SimplePipeClient();
	virtual ~SimplePipeClient();

	void SetPipePath(const string path) {
		_path = path;
	}

protected:
	virtual int Open();

private:
	string _path;
//	int _fd = 0; // default: stdin
};

} /* namespace gurum */

#endif /* APPS_SIMPLEIO_SIMPLECLIENT_H_ */
