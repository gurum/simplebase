/*
 * SimpleServer.h
 *
 *  Created on: Apr 13, 2016
 *      Author: buttonfly
 */

#ifndef GURUM_SIMPLESERVER_H_
#define GURUM_SIMPLESERVER_H_

#ifndef GCC_VERSION
#define GCC_VERSION (__GNUC__ * 10000 \
				 + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)
#endif

#define USE_CC_THREAD 1

#if GCC_VERSION > 40800
#define USE_CC_THREAD	1
#endif

#include <string>
#if USE_CC_THREAD
#include <thread>
#include <mutex>
#else
#include <pthread.h>
#endif

#include <sys/un.h>
#include <sys/socket.h>
#include <stdint.h>
#include <netinet/in.h>
#include <functional>


//#ifndef OVERRIDE
//#define OVERRIDE
//#endif

namespace gurum {

class SimpleServerDelegate {

public:
	virtual void connected(int sck){}
	virtual void disconnected(int sck){}
	virtual void read(int sck, uint8_t *buf, int len){}
};

class SimpleServer {

using OnConnected=std::function<void(int)>;
using OnDisconnected=std::function<void(int)>;
using OnRead=std::function<void(int,uint8_t*,int)>;

public:
	SimpleServer();
	virtual ~SimpleServer();

	void SetConnectedCallback(OnConnected on_connected) {
	  on_connected_=on_connected;
	}
	void SetDisconnectedCallback(OnDisconnected on_disconnected) {
	  on_disconnected_=on_disconnected;
	}
	void SetReadCallback(OnRead on_read) {
	  on_read_=on_read;
	}

	void SetCallback(OnConnected on_connected, OnDisconnected on_disconnected, OnRead on_read) {
	  SetConnectedCallback(on_connected);
	  SetDisconnectedCallback(on_disconnected);
	  SetReadCallback(on_read);
	}

	void setDelegate(SimpleServerDelegate *delegate) {
		_delegate = delegate;
	}
	virtual int Start();
	virtual void Stop();
	bool IsStarted() {
		return _started;
	}
	bool IsStopped() {
		return _stopped;
	}

	void SetTimeout(uint64_t ms) {
		_timeout = ms;
	}

//	virtual int Send(uint8_t *buf, size_t len) {fprintf(stderr, "not implemented\n"); return -1;}
  virtual int Send(uint8_t *buf, size_t len);
//	virtual int client() { return 0; }

  // You must not call Start();
  int ReadAndDispatch();

protected:
	virtual int Open()=0;
	virtual struct sockaddr *Sockaddr()=0;
	virtual size_t SockaddrLen()=0;
	void lock();
	void unlock();

protected:
	virtual void Run(int clnt);

#if USE_CC_THREAD
#else
	static void thr(void *arg);
#endif

protected:
	SimpleServerDelegate *_delegate;
	bool _stopped=false;
	bool _started=false;
#if USE_CC_THREAD
	std::thread _thread;
	std::recursive_mutex _lck;
#else
	pthread_t _thread;
	pthread_mutex_t _lck;
#endif
	int _sck;

	fd_set _readfds;
	int _fd_max;
	uint64_t _timeout;

	OnConnected on_connected_=nullptr;
	OnDisconnected on_disconnected_=nullptr;
	OnRead on_read_=nullptr;
};

class SimpleUnixServer : public SimpleServer {
public:
	SimpleUnixServer();
	virtual ~SimpleUnixServer();

	void SetUnixSockPath(const std::string &unix_sock_path) {
		unix_sock_path_ = unix_sock_path;
	}

	void Stop() override;

protected:
	int Open() override;
	struct sockaddr *Sockaddr() override;
	size_t SockaddrLen() override;

private:
	struct sockaddr_un  _serveraddr;
	struct sockaddr_un  _clntaddr;
	std::string unix_sock_path_;
};

class SimpleTcpServer : public SimpleServer {
public:
	SimpleTcpServer();
	virtual ~SimpleTcpServer();

	void SetPort(uint16_t port) {
		_port = port;
	}

//  virtual int Send(uint8_t *buf, size_t len) override;

protected:
	virtual int Open() override;
	struct sockaddr *Sockaddr() override;
	size_t SockaddrLen() override;

protected:
	struct sockaddr_in  _serveraddr;
	struct sockaddr_in _clntaddr;
	uint16_t _port;
};

class SimpleUdpServer : public SimpleTcpServer {
public:
	SimpleUdpServer();
	virtual ~SimpleUdpServer();

protected:
	virtual int Open() override;
};

class SimplePipeServer : public SimpleServer {
public:
	SimplePipeServer();
	virtual ~SimplePipeServer();

	void SetPipePath(const std::string pipePath) {
		_path = pipePath;
	}

	virtual int Send(uint8_t *buf, size_t len) override;

	void Stop() override;
	//@deprecated. It should not be used.
//	virtual int client() override { return _fd; }

protected:
	virtual void Run(int clnt) override;
	int Open() override;
	struct sockaddr *Sockaddr() override { return nullptr; };
	size_t SockaddrLen() override { return 0;};

private:
	std::string _path;
	int _fd=1; //default: stdout
	int _id=-1;
	int _wd=-1;
};

} /* namespace gurum */

#endif /* GURUM_SIMPLESERVER_H_ */
