/*
 * MulticastClient.h
 *
 *  Created on: May 1, 2016
 *      Author: buttonfly
 */

#ifndef MULTICASTCLIENT_H_
#define MULTICASTCLIENT_H_

#include <stdint.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <functional>
#include "simple_client.h"

//#ifndef OVERRIDE
//#define OVERRIDE
//#endif

#define UDP_BUFFER_SIZE	1316
#define RTP_BUFFER_SIZE 1328

namespace gurum {

class MulticastClient : public SimpleClient{
public:
	MulticastClient();
	virtual ~MulticastClient();

	void SetIP(std::string ip) {
		_ip = ip;
	}
	void SetPort(uint16_t port) {
		_port = port;
	}

protected:
	virtual int Open() override;
	virtual int Recv(int fd, uint8_t *buf, size_t len) override;
	virtual void Run() override;

private:
	std::string _ip;
	uint16_t _port;
	struct sockaddr_in _serveraddr;
	struct ip_mreq _mreq;
};

} /* namespace gurum */

extern "C" {
void *multicast_client_new();
void multicast_client_set_ip(void *, const char *);
void multicast_client_set_port(void *, unsigned short);
int multicast_client_start(void *);
void multicast_client_stop(void *);
void multicast_client_delete(void *);

void multicast_client_set_connected_callback(void *,std::function<void(void)>);
void multicast_client_set_disconnected_callback(void *,std::function<void(void)>);
void multicast_client_set_hook_callback(void *,std::function<void(void)>);
void multicast_client_set_read_callback(void *,std::function<void(void)>);
}

#endif /* MULTICASTCLIENT_H_ */
