/*
 * MulticastServer.h
 *
 *  Created on: May 1, 2016
 *      Author: buttonfly
 */

#ifndef MULTICASTSERVER_H_
#define MULTICASTSERVER_H_

#include <stdio.h>
#include <stdint.h>
#include <string>
#include <sys/socket.h>
#include <stdint.h>
#include <netinet/in.h>

#include "simple_server.h"

//#ifndef OVERRIDE
//#define OVERRIDE
//#endif

#define UDP_BUFFER_SIZE	1316
#define RTP_BUFFER_SIZE 1328

// UDP | RTP | UNIX?
namespace gurum {

class MulticastServer : public SimpleServer {
public:
	MulticastServer();
	virtual ~MulticastServer();

	void SetIP(std::string ip) {
		_ip = ip;
	}
	void SetPort(uint16_t port) {
		_port = port;
	}
	void SetTTL(uint8_t ttl) {
		_ttl = ttl;
	}
	void SetLoop(uint8_t loop) {
		_loop = loop;
	}
	void SetAlias(std::string alias) {
		_alias = alias;
	}

	int Send(uint8_t *buf, size_t len) override;
	void Run(int clnt) override;

private:
	int Open() override;
	struct sockaddr *Sockaddr() override;
	size_t SockaddrLen() override;

private:
	uint8_t _ttl;
	uint8_t _loop;
	std::string _ip;
	std::string _alias;
	uint16_t _port;

	struct sockaddr_in _serveraddr;
	struct in_addr _iaddr;
	struct sockaddr_in _clntaddr;
};

} /* namespace gurum */

extern "C" {

void *multicast_server_new();
void multicast_server_set_ip(void *handle, const char *addr);
void multicast_server_set_port(void *handle, uint16_t port);
void multicast_server_set_alias(void *handle, const char *alias);
void multicast_server_set_ttl(void *handle, uint8_t ttl);
void multicast_server_set_loop(void *handle, uint8_t loop);
int multicast_server_start(void *handle);
void multicast_server_stop(void *handle);
void multicast_server_delete(void *handle);

void multicast_server_set_connected_callback(void *handle,std::function<void(int)>);
void multicast_server_set_disconnected_callback(void *handle,std::function<void(int)>);
void multicast_server_set_read_callback(void *handle,std::function<void(int,uint8_t*,int)>);
}

#endif /* MULTICASTSERVER_H_ */

