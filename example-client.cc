/*
 * example.cc
 *
 *  Created on: Oct 1, 2017
 *      Author: buttonfly
 */

#include <simpleio/simple_client.h>

#include <memory>
#include <unistd.h>
#include <stdio.h>
#include <simplethread/simple_thread.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <functional>
#include <sstream>

int main(int argc, char *argv[]) {
  std::unique_ptr<base::SimpleThread> thr{new base::SimpleThread};

  thr->Init();
  thr->Start();


  std::unique_ptr<gurum::SimpleUnixClient> clnt{new gurum::SimpleUnixClient};
  clnt->SetTimeout(10);
  clnt->SetUnixSockPath(".sck");
  clnt->SetCallback([](){
    fprintf(stderr, "connected\n");
  },
  [&]() {
    fprintf(stderr, "disconnected\n");
    clnt->Stop();
  },
  [](int sck, int len)->bool{
    fprintf(stderr, "hook\n");
    return false; // if true, [read] wont' be called
  },
  [](uint8_t* buf, int len) {
    fprintf(stderr, "read: %s\n", buf);
  });

  for(;;) {
    std::string c;
    thr->PostTask([&](){
      clnt->ReadAndDispatch();
    });
    std::getline(std::cin, c);
    clnt->Send((uint8_t *)c.c_str(), c.size());
  }

  thr->Stop();
  thr.reset(nullptr);
  return 0;
}
