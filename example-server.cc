/*
 * example.cc
 *
 *  Created on: Oct 1, 2017
 *      Author: buttonfly
 */


#include <simplethread/simple_thread.h>
#include <simpleio/simple_server.h>

#include <memory>
#include <unistd.h>
#include <stdio.h>
#include <simplethread/simple_thread.h>
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <functional>
#include <sstream>

int main(int argc, char *argv[]) {
  std::unique_ptr<base::SimpleThread> thr{new base::SimpleThread};

  thr->Init();
  thr->Start();


  std::unique_ptr<gurum::SimpleUnixServer> server{new gurum::SimpleUnixServer};
  server->SetUnixSockPath(".sck");
  server->SetTimeout(10);
  server->SetCallback([](int sck){
    fprintf(stderr, "connected\n");
  },
  [] (int) {
    fprintf(stderr, "disconnected\n");
  },
  [&](int sck, uint8_t *buf, int len) {
    fprintf(stderr, "read %s\n", buf);
    server->Send(buf, len);
  });

  for(;;) {
    std::string c;
    thr->PostTask([&](){
      server->ReadAndDispatch();
    });
  }

  thr->Stop();
  thr.reset(nullptr);
  return 0;
}
